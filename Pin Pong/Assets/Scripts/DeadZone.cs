﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class DeadZone : MonoBehaviour
{
    public Text scorePlayerText;
    public Text scoreEnemyText;

    public int scorePlayerQuantity;
    public int scoreEnemyQuantity;




    private void OnTriggerEnter2D(Collider2D ball)
    {
        if (gameObject.tag == "Izquierda")
        {
            scoreEnemyQuantity++;
            UpdateScoreLabel(scoreEnemyText, scoreEnemyQuantity);

        }
        else if (gameObject.CompareTag("Derecha"))
        {
            scorePlayerQuantity++;
            UpdateScoreLabel(scorePlayerText, scorePlayerQuantity);
        }


      



    }

    void UpdateScoreLabel(Text label, int score)
    {
        label.text = score.ToString();
    }
}

